json.array!(@punishments) do |punishment|
  json.extract! punishment, :id, :name
  json.url punishment_url(punishment, format: :json)
end
